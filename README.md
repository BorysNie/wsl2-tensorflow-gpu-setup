### Note
This installation script was designed to work with WSL2 Ubuntu-22.04 LTS on x86_64

### Setup
Download Operating system Cuda drivers and install them

https://developer.nvidia.com/cuda-downloads

Run install.sh as a super user

#### Resources
- https://developer.download.nvidia.com/compute/cuda/repos/
- https://developer.download.nvidia.com/compute/machine-learning/repos/
- https://github.com/nvidia/cuda-samples
- https://www.tensorflow.org/api_docs/python/tf/test/is_gpu_available
- https://docs.nvidia.com/cuda/wsl-user-guide/index.html

![image.png](./image.png)
