#!/usr/bin/python3
import os
import tensorflow as tf

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
physical_devices = len(tf.config.list_physical_devices("GPU"))

print(f"Available GPUs: {physical_devices}")
