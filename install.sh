#!/bin/bash
# Apt update and dependency installation
sudo apt-get update
sudo apt-get install -y gcc make

# Nvidia repo settings
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/3bf863cc.pub
sudo sh -c 'echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64 /" > /etc/apt/sources.list.d/cuda.list'

# Install CUDE toolkit
sudo apt-get update
wget https://developer.download.nvidia.com/compute/cuda/11.7.1/local_installers/cuda_11.7.1_515.65.01_linux.run
echo 'Installing toolkit...'
sudo sh cuda_11.7.1_515.65.01_linux.run --silent --driver
sudo apt-get install -y cuda-toolkit-11-7

# Patch CUDA libraries
wget https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/libcudnn8_8.0.4.30-1+cuda11.0_amd64.deb
sudo apt-get install ./libcudnn8_8.0.4.30-1+cuda11.0_amd64.deb
wget https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/libcudnn8-dev_8.0.4.30-1+cuda11.0_amd64.deb
sudo apt-get install ./libcudnn8-dev_8.0.4.30-1+cuda11.0_amd64.deb

# Install python3 and python3-pip
sudo apt-get install -y python3
sudo apt-get install -y python3-pip

# Install tensorflow
pip install tensorflow-gpu --no-warn-script-location

# Run tensorflow GPU test
echo ''
nvidia-smi
echo ''
python3 $PWD/tf-gpu-test.py
echo ''

# Remove all created files
rm -f cuda_11.7.1_515.65.01_linux.run
rm -f libcudnn8*
